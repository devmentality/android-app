package com.example.a1.myapplication;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Message;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.fasterxml.jackson.databind.ObjectMapper;


/*
 * Created by devmentality on 20.03.2018.
 */

public class ItemPresenter implements RestorablePresenter{
    private HttpClient model;
    private String templateUrlToComputer = "http://testwork.nsd.naumen.ru/rest/computers/%d";
    private String templateUrlToSimilarItems = "http://testwork.nsd.naumen.ru/rest/computers/%d/similar";
    private ItemViewInterface view;

    private int itemId;
    private ComputerCard card;
    private Bitmap image;
    private Computer[] similarities;
    private boolean errorOccurred;
    private String errorMessage;

    private static class onFinishedLoadingCardInfoHandler extends ItemPresenterOnFinishedHandler
    {
        public onFinishedLoadingCardInfoHandler(ItemPresenter presenter, ItemViewInterface view)
        {
            super(presenter, view);
        }

        @Override
        public void handleMessage(Message msg)
        {
            ItemPresenter presenter = presenterRef.get();
            ItemViewInterface view = viewRef.get();
            // Check whether references on presenter or view are destroyed
            if (presenter == null || view == null)
                return;

            view.hideProgressItemLoading();
            if (msg.what == HttpClient.SUCCESSFUL_LOADING)
            {
                Bundle data = msg.getData();
                presenter.onLoadingCardInfoFinished(data.getString("content"));
            }
            else
                presenter.processError("Unable to connect to the server.");
        }
    }

    private static class onFinishedLoadingImageHandler extends ItemPresenterOnFinishedHandler
    {
        public onFinishedLoadingImageHandler(ItemPresenter presenter, ItemViewInterface view)
        {
            super(presenter, view);
        }

        public void handleMessage(Message msg)
        {
            ItemPresenter presenter = presenterRef.get();
            ItemViewInterface view = viewRef.get();
            if (presenter == null || view == null)
                return;

            view.hideProgressImageLoading();
            if (msg.what == HttpClient.SUCCESSFUL_LOADING)
            {
                Bitmap image = (Bitmap) msg.obj;
                presenter.onLoadingImageFinished(image);
            }
        }
    }

    private static class onFinishedLoadingSimilarItemsHandler extends ItemPresenterOnFinishedHandler
    {
        public onFinishedLoadingSimilarItemsHandler(ItemPresenter presenter, ItemViewInterface view)
        {
            super(presenter, view);
        }

        public void handleMessage(Message msg)
        {
            ItemPresenter presenter = presenterRef.get();
            ItemViewInterface view = viewRef.get();
            if (presenter == null || view == null)
                return;

            if (msg.what == HttpClient.SUCCESSFUL_LOADING)
            {
                Bundle data = msg.getData();
                presenter.onLoadingSimilarItemsFinished(data.getString("content"));
            }
        }
    };

    public ItemPresenter(HttpClient model, ItemViewInterface view, int itemId)
    {
        this.model = model;
        this.view = view;
        this.itemId = itemId;
        errorOccurred = false;
        errorMessage = null;
        card = null;
        image = null;
        similarities = null;
        requestCardPage();
    }

    @Override
    public void onRestore(RestorableViewInterface view) {
        this.view = (ItemViewInterface) view;
        if (errorOccurred)
        {
            this.view.showError(errorMessage);
            return;
        }

        if (card != null)
            this.view.showComputerCard(card);
        if (image != null)
            this.view.showImage(image);
        if(similarities != null)
            this.view.showSimilarItems(similarities);
    }

    private void requestCardPage()
    {
        disableError();
        view.showProgressItemLoading();
        String url = String.format(Locale.ENGLISH, templateUrlToComputer, itemId);
        model.loadUrlAsPlainText(new onFinishedLoadingCardInfoHandler(this, view), url);
    }

    public void onLoadingCardInfoFinished(String content) {
        try
        {
            card = parseCardJSON(content);
            view.showComputerCard(card);
        }
        catch (IOException ex)
        {
            processError("Unable to process data from server.");
            card = null;
            return;
        }

        if (card.imageUrl != null)
        {
            model.downloadImage(new onFinishedLoadingImageHandler(this, view), card.imageUrl);
            view.showProgressImageLoading();
        }

        String urlToSimilar = String.format(Locale.ENGLISH, templateUrlToSimilarItems, card.id);
        model.loadUrlAsPlainText(new onFinishedLoadingSimilarItemsHandler(this, view), urlToSimilar);
    }

    public void onLoadingImageFinished(Bitmap image)
    {
        view.hideProgressImageLoading();
        this.image = image;
        view.showImage(image);
    }

    public void onLoadingSimilarItemsFinished(String content) {
        try
        {
            similarities = parseSimilarItemsJSON(content);
            view.showSimilarItems(similarities);
        }
        catch (IOException ex){}
    }

    private ComputerCard parseCardJSON(String content) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        ComputerCard card = mapper.readValue(content, ComputerCard.class);

        if (card.introduced != null)
        {
            try
            {
                card.introduced = convertDateFormat(card.introduced);
            }
            catch (ParseException ex) {}
        }
        if (card.discounted != null)
        {
            try
            {
                card.discounted = convertDateFormat(card.discounted);
            }
            catch (ParseException ex) {}
        }
        return card;
    }

    private String convertDateFormat(String dateString) throws ParseException
    {
        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat newFormat = new SimpleDateFormat("dd MMM yyyy");

        Date date = oldFormat.parse(dateString);
        return newFormat.format(date);
    }

    private Computer[] parseSimilarItemsJSON(String content) throws IOException
    {
        return (new ObjectMapper()).readValue(content, Computer[].class);
    }

    private void processError(String error)
    {
        errorOccurred = true;
        errorMessage = error;
        view.showError(errorMessage);
    }

    private void disableError()
    {
        errorOccurred = false;
        errorMessage = null;
        view.hideError();
    }
}
