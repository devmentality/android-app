package com.example.a1.myapplication;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;


public class ItemActivity extends AppCompatActivity implements ItemViewInterface {
    private RestorablePresenterFragment fragment;
    private LinearLayout infoLayout;
    private LinearLayout similaritiesLayout;
    private ImageView imageView;
    private int itemId;
    private ItemPresenter presenter;
    private ProgressBar itemProgressBar;
    private ProgressBar imageProgressBar;
    private TextView itemErrorMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);

        infoLayout = findViewById(R.id.infoLayout);
        imageView = findViewById(R.id.image);
        similaritiesLayout = findViewById(R.id.similarities);
        itemProgressBar = findViewById(R.id.itemProgressBar);
        imageProgressBar = findViewById(R.id.imageProgressBar);
        itemErrorMessage = findViewById(R.id.itemErrorMessage);

        extractInfoFromIntent(getIntent());
        restoreState();
    }

    private void extractInfoFromIntent(Intent intent)
    {
        itemId = intent.getIntExtra("id", 0);
        String compName = intent.getStringExtra("name");
        setTitle(compName);
    }

    private void restoreState()
    {
        FragmentManager fragmentManager = getFragmentManager();
        fragment = (RestorablePresenterFragment) fragmentManager.findFragmentByTag("ItemPresenter");

        if (fragment == null)
        {
            fragment = new RestorablePresenterFragment();
            presenter = new ItemPresenter(HttpClient.getItem(), this, itemId);
            fragmentManager.beginTransaction().add(fragment, "ItemPresenter").commit();
        }
        else
        {
            presenter = (ItemPresenter) fragment.getPresenter();
            presenter.onRestore(this);
        }
    }

    private void addTextView(String description, String content)
    {
        DescriptionTextView text = new DescriptionTextView(this, 100);
        text.setPadding(30, 10, 10, 0);
        text.setTextColor(0xFF000000);
        text.setTextSize(14);
        text.setDescription(content);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((DescriptionTextView)view).ExpandText();
            }
        });

        TextView desc = new TextView(this);
        desc.setPadding(60,0,10,10);
        desc.setTextColor(0xFF707070);
        desc.setTextSize(14);
        desc.setText(description);

        infoLayout.addView(text);
        infoLayout.addView(desc);
    }

    @Override
    public void showComputerCard(ComputerCard card) {
        if (card.company != null)
            addTextView("Company", card.company.name);
        if (card.introduced != null)
            addTextView("Introduced", card.introduced);
        if(card.discounted != null)
            addTextView("Discounted", card.discounted);
        if (card.description != null)
            addTextView("Description" , card.description);
    }

    @Override
    public void showImage(Bitmap image) {
        imageView.setImageBitmap(image);
    }

    @Override
    public void showSimilarItems(Computer[] computers) {
        TextView label = new TextView(this);
        label.setText(R.string.similar_label);
        label.setTextColor(0xFF707070);
        label.setTextSize(14);
        label.setGravity(Gravity.CENTER);
        similaritiesLayout.addView(label);

        for(Computer computer: computers)
        {
            SimilaritiesTextView listItem = new SimilaritiesTextView(this, computer);
            listItem.setText(computer.name);
            listItem.setPadding(30,0,0,0);
            listItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ItemActivity.this, ItemActivity.class);
                    intent.putExtra("id", ((SimilaritiesTextView)view).getComputerId());
                    intent.putExtra("name", ((SimilaritiesTextView)view).getComputerName());
                    startActivity(intent);
                }
            });
            similaritiesLayout.addView(listItem);
        }
    }

    @Override
    public void showProgressItemLoading() {
        itemProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressItemLoading() {
        itemProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showProgressImageLoading() {
        imageProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressImageLoading() {
        imageProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage) {
        itemErrorMessage.setText(errorMessage);
        itemErrorMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError() {
        itemErrorMessage.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment.setPresenter(presenter);
    }
}
