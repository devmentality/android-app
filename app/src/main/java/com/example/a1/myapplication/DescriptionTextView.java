package com.example.a1.myapplication;

/*
 * Created by devmentality on 24.03.2018.
 */

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;

public class DescriptionTextView extends AppCompatTextView{
    private String text;
    private int symbolsCount;
    private Context context;

    DescriptionTextView(Context context)
    {
        super(context);
        this.context = context;
        text = "";
        symbolsCount = 100;
    }

    DescriptionTextView(Context context, int count)
    {
        this(context);
        symbolsCount = count;
    }

    public void setDescription(String text)
    {
        this.text = text;
        if(text.length() > symbolsCount)
            super.setText(context.getString(R.string.description_ending, text.substring(0, symbolsCount)));
        else
            super.setText(text);
    }

    public void ExpandText()
    {
        super.setText(text);
    }
}
