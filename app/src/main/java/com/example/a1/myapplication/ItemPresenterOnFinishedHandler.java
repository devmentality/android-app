package com.example.a1.myapplication;

/*
 * Created by devmentality on 27.03.2018.
 */

import android.os.Message;

import java.lang.ref.WeakReference;
import android.os.Handler;

public abstract class ItemPresenterOnFinishedHandler extends Handler {
    WeakReference<ItemPresenter> presenterRef;
    WeakReference<ItemViewInterface> viewRef;

    public ItemPresenterOnFinishedHandler(ItemPresenter presenter, ItemViewInterface view) {
        presenterRef = new WeakReference<>(presenter);
        viewRef = new WeakReference<>(view);
    }

    @Override
    public abstract void handleMessage(Message msg);
}
