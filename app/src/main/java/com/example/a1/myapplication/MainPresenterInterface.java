package com.example.a1.myapplication;

/*
 * Created by devmentality on 16.03.2018.
 */

public interface MainPresenterInterface {
    void onNextButtonClicked();
    void onPrevButtonClicked();
    void onRestore(MainViewInterface view);
}
