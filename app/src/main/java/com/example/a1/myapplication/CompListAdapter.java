package com.example.a1.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/*
 * Created by devmentality on 23.03.2018.
 */

public class CompListAdapter extends BaseAdapter {
    private Context context;
    private Computer[] computers;
    private LayoutInflater inflater;

    CompListAdapter(Context context, Computer[] computers)
    {
        this.context = context;
        this.computers = computers;
        inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return computers.length;
    }

    @Override
    public Object getItem(int i) {
        return computers[i];
    }

    @Override
    public long getItemId(int i) {
        return computers[i].id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View newView = view;
        if (newView == null)
            newView = inflater.inflate(R.layout.comp_item, viewGroup, false);

        Computer computer = computers[i];

        ((TextView) newView.findViewById(R.id.name)).setText(computer.name);
        if (computer.company != null)
            ((TextView) newView.findViewById(R.id.company)).setText(computer.company.name);
        else
        {
            newView.findViewById(R.id.company).setVisibility(View.GONE);
            newView.findViewById(R.id.companyLabel).setVisibility(View.GONE);
        }

        return newView;
    }
}
