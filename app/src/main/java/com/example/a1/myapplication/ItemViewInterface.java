package com.example.a1.myapplication;

import android.graphics.Bitmap;

/*
 * Created by devmentality on 20.03.2018.
 */

public interface ItemViewInterface extends RestorableViewInterface{
    void showComputerCard(ComputerCard card);
    void showImage(Bitmap image);
    void showSimilarItems(Computer[] computers);

    void showProgressItemLoading();
    void hideProgressItemLoading();

    void showProgressImageLoading();
    void hideProgressImageLoading();

    void showError(String errorMessage);
    void hideError();
}
