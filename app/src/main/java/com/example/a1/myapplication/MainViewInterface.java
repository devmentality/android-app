package com.example.a1.myapplication;

/*
 * Created by devmentality on 16.03.2018.
 */

public interface MainViewInterface extends RestorableViewInterface{
    void showInstances(Computer[] computers);
    void hideInstances();

    void showError(String errorMessage);
    void hideError();

    void showProgressBar();
    void hideProgressBar();

    void enableNextButton(boolean state);
    void enablePrevButton(boolean state);
    boolean isNextButtonEnabled();
    boolean isPrevButtonEnabled();
}
