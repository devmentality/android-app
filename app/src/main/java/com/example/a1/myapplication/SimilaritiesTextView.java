package com.example.a1.myapplication;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;

/*
 * Created by devmentality on 24.03.2018.
 */

public class SimilaritiesTextView extends AppCompatTextView{
    private Computer computer;

    SimilaritiesTextView(Context context, Computer computer)
    {
        super(context);
        this.computer = computer;
    }
    int getComputerId()
    {
        return computer.id;
    }

    String getComputerName()
    {
        return computer.name;
    }
}
