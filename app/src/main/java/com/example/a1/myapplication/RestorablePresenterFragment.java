package com.example.a1.myapplication;

/*
 * Created by devmentality on 26.03.2018.
 */

import android.app.Fragment;
import android.os.Bundle;

public class RestorablePresenterFragment extends Fragment{
    private RestorablePresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setPresenter(RestorablePresenter presenter) {
        this.presenter = presenter;
    }

    public RestorablePresenter getPresenter() {
        return presenter;
    }
}
