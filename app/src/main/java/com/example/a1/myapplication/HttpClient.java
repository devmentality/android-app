package com.example.a1.myapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/*
 * Created by devmentality on 15.03.2018.
 */

public class HttpClient {
    private static final HttpClient instance = new HttpClient();
    private OkHttpClient client;
    private HttpClient()
    {
        client = new OkHttpClient();
    }

    public static int SUCCESSFUL_LOADING = 1;
    public static int LOADING_FAILED = 0;

    public static HttpClient getItem()
    {
        return instance;
    }

    private Response getResponse(final String url) throws IOException
    {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        return response;
    }

    public void downloadImage(final Handler onFinishedHandler, final String imageUrl)
    {
        Thread downloadImageTask = new Thread()
        {
            @Override
            public void run()
            {
                Message msg = new Message();
                InputStream imageStream;
                try
                {
                    Response response = getResponse(imageUrl);
                    if (response.isSuccessful())
                    {
                        imageStream = response.body().byteStream();
                        Bitmap image = BitmapFactory.decodeStream(imageStream);
                        msg.what = SUCCESSFUL_LOADING;
                        msg.obj = image;
                    }
                    else
                    {
                        msg.what = LOADING_FAILED;
                    }
                }
                catch (IOException|java.lang.NullPointerException ex1)
                {
                    msg.what = LOADING_FAILED;
                }
                onFinishedHandler.sendMessage(msg);
            }
        };

        downloadImageTask.start();
    }

    public void loadUrlAsPlainText(final Handler onFinishedHandler, final String url)
    {
        Thread getUrlTask = new Thread()
        {
            @Override
            public void run()
            {
                Message msg = new Message();
                try {
                    Response response = getResponse(url);
                    if (response.isSuccessful())
                    {
                        String stringResponse = response.body().string();

                        Bundle bundle = new Bundle();
                        bundle.putString("content", stringResponse);

                        msg.what = SUCCESSFUL_LOADING;
                        msg.setData(bundle);
                    }
                    else
                    {
                        msg.what = LOADING_FAILED;
                    }
                }
                catch (IOException|java.lang.NullPointerException ex) {
                    msg.what = LOADING_FAILED;
                }
                onFinishedHandler.sendMessage(msg);
            }
        };
        getUrlTask.start();
    }
}