package com.example.a1.myapplication;

import android.os.Bundle;
import android.os.Handler;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Locale;
import java.util.HashMap;

/*
 * Created by devmentality on 16.03.2018.
 */

public class MainPresenter implements RestorablePresenter
{
    private HttpClient model;
    private MainViewInterface view;

    private int currentPageNumber;
    private boolean lastPageFlag;
    private boolean firstPageFlag;
    private Computer[] computers;
    private int offset;
    private int total;
    private boolean errorOccurred = false;
    private String errorMessage = null;

    private static class onFinishedLoadingPageHandler extends Handler
    {
        WeakReference<MainPresenter> presenterRef;
        WeakReference<MainViewInterface> viewRef;

        public onFinishedLoadingPageHandler(MainPresenter presenter, MainViewInterface view)
        {
            presenterRef = new WeakReference<>(presenter);
            viewRef = new WeakReference<>(view);
        }

        @Override
        public void handleMessage(android.os.Message msg)
        {
            MainViewInterface view = viewRef.get();
            MainPresenter presenter = presenterRef.get();
            if (presenter == null || view == null)
                return;

            view.hideProgressBar();
            if (msg.what == HttpClient.SUCCESSFUL_LOADING)
            {
                Bundle data = msg.getData();
                presenter.onLoadingPageFinished(data.getString("content"));
            }
            else
                presenter.processError("Unable to connect to the server.");
        }
    };

    public MainPresenter(HttpClient model, MainViewInterface view)
    {
        this.model = model;
        this.view = view;

        currentPageNumber = 0;
        firstPageFlag = false;
        lastPageFlag = false;
        computers = null;
        total = 0;
        offset = 0;
        errorOccurred = false;
        errorMessage = null;

        requestLoadingCurrentPage();
    }

    @Override
    public void onRestore(RestorableViewInterface view)
    {
        this.view = (MainViewInterface)view;
        if (errorOccurred)
            this.view.showError(errorMessage);
        else if(computers == null)
            this.view.showError("Unable to restore page.");
        else
            this.view.showInstances(computers);
        checkButtons();
    }

    public void onNextButtonClicked()
    {
        currentPageNumber++;
        requestLoadingCurrentPage();
    }

    public void onPrevButtonClicked()
    {
        currentPageNumber--;
        requestLoadingCurrentPage();
    }

    private void requestLoadingCurrentPage()
    {
        view.hideInstances();
        view.showProgressBar();
        view.enablePrevButton(false);
        view.enableNextButton(false);

        String url = String.format(
                Locale.ENGLISH,
                "http://testwork.nsd.naumen.ru/rest/computers?p=%d",
                currentPageNumber);
        model.loadUrlAsPlainText(new onFinishedLoadingPageHandler(this, view), url);
    }

    private void onLoadingPageFinished(String content)
    {
        disableError();
        if (content == null)
        {
            processError("Unable to load data from server.");
            return;
        }

        try
        {
            HashMap<String, Object> parsingResult = parseJSON(content);
            computers = (Computer[]) parsingResult.get("computers");
            offset = (int) parsingResult.get("offset");
            total = (int) parsingResult.get("total");

            lastPageFlag = offset + computers.length >= total;
            firstPageFlag = offset == 0;

            view.showInstances(computers);
            checkButtons();
        }
        catch (IOException ex)
        {
            computers = null;
            processError("Unable to process data from server.");
        }
    }

    private void processError(String error)
    {
        errorOccurred = true;
        errorMessage = error;
        view.showError(errorMessage);
    }

    private void disableError()
    {
        errorOccurred = false;
        errorMessage = null;
        view.hideError();
    }

    private HashMap<String, Object> parseJSON(String content) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();

        JsonNode jsonRoot = mapper.readTree(content);
        String itemsListJson = jsonRoot.at("/items").toString();
        int offset = jsonRoot.at("/offset").asInt();
        int total = jsonRoot.at("/total").asInt();

        Computer[] computers = mapper.readValue(itemsListJson, Computer[].class);
        HashMap<String, Object> result = new HashMap<>();
        result.put("computers", computers);
        result.put("offset", offset);
        result.put("total", total);

        return result;
    }

    private void checkButtons()
    {
        if (lastPageFlag && view.isNextButtonEnabled())
            view.enableNextButton(false);
        else if (!lastPageFlag && !view.isNextButtonEnabled())
            view.enableNextButton(true);

        if (firstPageFlag && view.isPrevButtonEnabled())
            view.enablePrevButton(false);
        else if (!firstPageFlag && !view.isPrevButtonEnabled())
            view.enablePrevButton(true);
    }
}
