package com.example.a1.myapplication;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements MainViewInterface {
    private RestorablePresenterFragment fragment;
    private ListView lvMain;
    private MainPresenter presenter;
    private Button prevButton;
    private Button nextButton;
    private TextView errorMessages;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lvMain = findViewById(R.id.lvMain);
        prevButton = findViewById(R.id.prevButton);
        nextButton = findViewById(R.id.nextButton);
        errorMessages = findViewById(R.id.errorMessage);
        progressBar = findViewById(R.id.loadingProgressBar);

        prevButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onPrevButtonClicked();
                }
            }
        );

        nextButton.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.onNextButtonClicked();
                }
            }
        );

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Computer computer = (Computer) adapterView.getItemAtPosition(i);
                Intent intent = new Intent(MainActivity.this, ItemActivity.class);
                intent.putExtra("id", computer.id);
                intent.putExtra("name", computer.name);
                startActivity(intent);
            }
        });

        FragmentManager fragmentManager = getFragmentManager();
        fragment = (RestorablePresenterFragment) fragmentManager.findFragmentByTag("MainPresenter");
        if (fragment == null)
        {
            fragment = new RestorablePresenterFragment();
            presenter = new MainPresenter(HttpClient.getItem(), this);
            fragmentManager.beginTransaction().add(fragment, "MainPresenter").commit();
        }
        else
        {
            presenter = (MainPresenter) fragment.getPresenter();
            presenter.onRestore(this);
        }
    }

    @Override
    public void showInstances(Computer[] computers){
        CompListAdapter adapter = new CompListAdapter(this, computers);
        lvMain.setAdapter(adapter);
        lvMain.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideInstances()
    {
        lvMain.setVisibility(View.GONE);
    }

    @Override
    public void showError(String errorMessage){
        errorMessages.setText(errorMessage);
        errorMessages.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideError()
    {
        errorMessages.setVisibility(View.GONE);
    }

    @Override
    public void enableNextButton(boolean state)
    {
        nextButton.setEnabled(state);
    }

    @Override
    public void enablePrevButton(boolean state)
    {
        prevButton.setEnabled(state);
    }

    @Override
    public boolean isNextButtonEnabled()
    {
        return nextButton.isEnabled();
    }

    @Override
    public boolean isPrevButtonEnabled()
    {
        return prevButton.isEnabled();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        fragment.setPresenter(presenter);
    }
}
