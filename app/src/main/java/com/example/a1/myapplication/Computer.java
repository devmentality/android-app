package com.example.a1.myapplication;

/*
 * Created by devmentality on 20.03.2018.
 */

public class Computer {
    public int id;
    public String name;
    public Company company;

    @Override
    public String toString()
    {
        return String.format("Name: %s \nCompany: %s\n", name, company);
    }
}
