package com.example.a1.myapplication;

/*
 * Created by devmentality on 20.03.2018.
 */

public class Company {
    public int id;
    public String name;

    @Override
    public String toString()
    {
        return String.format("Name: %s", name);
    }
}
