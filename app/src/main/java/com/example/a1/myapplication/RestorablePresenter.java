package com.example.a1.myapplication;

/*
 * Created by devmentality on 26.03.2018.
 */

import android.app.Activity;

public interface RestorablePresenter {
    public void onRestore(RestorableViewInterface view);
}
