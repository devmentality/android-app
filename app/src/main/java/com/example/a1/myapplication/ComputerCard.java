package com.example.a1.myapplication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/*
 * Created by devmentality on 20.03.2018.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class ComputerCard {
    public int id = -1;
    public String name = null;
    public String imageUrl = null;
    public Company company = null;
    public String description = null;
    public String introduced = null; // Raw string, which represents date.
    public String discounted = null; // Raw string, which represents date.
}
